import React from 'react';
import logo from './logo.svg';
import './App.css';
import * as xlsx from 'xlsx'; 


function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          대준이형을 위한 엑셀 매크로!!
        </p>
        <input type="file" id='xlsxSelector' accept="*.xlsx,*.xls"></input>
        <button id="converse" onClick={xlsxParser}>보내기</button>
      </header>
    </div>
  );
}

async function xlsxParser() {
  try {
    const xlsxFileInput = document.getElementById('xlsxSelector') as HTMLInputElement;
    if (!xlsxFileInput.files || xlsxFileInput.files.length === 0) {
      alert('파일을 선택해주세요.');
      return;
    }

    const xlsxFileText = await xlsxFileInput.files[0].arrayBuffer();
    const parseXlsx = xlsx.read(xlsxFileText, {type: 'buffer'});
    const returnXlsx = xlsx.utils.book_new();

    for(const sheetKey in parseXlsx.Sheets){
      const sheet: xlsx.WorkSheet = parseXlsx.Sheets[sheetKey];

      if (!sheet.hasOwnProperty('A1')) {
        continue;
      }
      const conSheet = xlsx.utils.json_to_sheet(parseRow(sheet));
      conSheet["!cols"] = [
        { wpx : 130 },  // A열
        { wpx : 100 },  // B열
        { wpx : 700 },   // C열
      ]
      xlsx.utils.book_append_sheet( returnXlsx, conSheet, sheetKey );
    }

    xlsx.writeFile( returnXlsx, "return.xlsx" ); 

  } catch (e) {
    console.log(e);
    alert('변환중 오류 발생.');
  }
}

function parseRow(sheet: xlsx.WorkSheet) {
  let saveConRow:Array<Object> = [];
  let saveTemp:{[key: string]: Object | null} = {};
  let saveTempSize:number = 1;
  
  for(const rowKey in sheet){
    const rowInitial: string = rowKey.replace(/[^a-zA-Z]/g, '');
    const row = sheet[rowKey];
    
    if (rowKey.includes('margins') || rowKey.includes('proto') || rowKey.includes('ref')) {
      continue;
    }

    // console.log(`${rowInitial} / ${rowIndex} / ${saveTempSize}`);

    if (saveTempSize === 4) {
      // console.log(`C: ${saveTempSize}`);
      saveConRow.push(saveTemp);
      saveTemp = {};
      saveTempSize = 1;
    } 

    // console.log(saveTempSize);
    
    if (rowInitial === "A" || rowInitial === "B") {
      saveTemp[rowInitial] = row.v;
    } else if (rowInitial === "C") {
      saveTemp[rowInitial] = areaParser(row.v);
    } else {
      alert(`다른 유형의 로우 :: ${rowInitial}, 작성자에게 문의해주길 바랍니다.`);
      break;
    }
    saveTempSize++;
  }

  saveConRow.push(saveTemp);
  return saveConRow;
}

function areaParser(area: string) {
  let splitArea: Array<string> = area.split(',');
  let converseArea: string = '';
  let areaInitial: string = '';
  const regExp: RegExp = /[a-zA-Z]/;
  const regWhite: RegExp = /[\n|\r]/g;

  // B2~B4,B6~B9,11,\r\n12,16~18,B27~B29
  for(let oneArea of splitArea) {
    let prefix = '';
    let saveArea: Array<string> = [];
    
    if (oneArea.includes('\n')) {
      prefix = '\r\n';
      oneArea = oneArea.replace(regWhite, '');
    }

    if (regExp.test(oneArea)) { // 구역 알파벳 저장
      if (oneArea.includes('~')) {
        console.log('split test :: ' + oneArea.split('~'));
        if (regExp.test(oneArea.split('~')[0])) {
          areaInitial = oneArea.split('~')[0].replace(/[^a-zA-Z]/g, '');
          console.log(`0번 :: ${areaInitial}`);
        } else if (regExp.test(oneArea.split('~')[1])) {
          areaInitial = oneArea.split('~')[1].replace(/[^a-zA-Z]/g, '');
          console.log(`1번 :: ${areaInitial}`);
        }
      } else {
        areaInitial = oneArea.replace(/[^a-zA-Z]/g, '');
      }
    }

    if (oneArea.includes('~')) { // ~ 범위지정
      const startArea: number = parseInt(oneArea.split('~')[0].replace(/[^0-9]/g, ''));
      const endArea: number = parseInt(oneArea.split('~')[1].replace(/[^0-9]/g, ''));

      for(let i:number=startArea; i<=endArea; i++) {
        saveArea.push(`${areaInitial}${i}`);
      }

    } else if (!regExp.test(oneArea)) {
      const onlyNum: number = parseInt(oneArea.replace(/[^0-9]/g, ''));
      saveArea.push(`${areaInitial}${onlyNum}`);
    } else {
      saveArea.push(`${oneArea}`);
    }

    saveArea[0] = prefix + saveArea[0];
    converseArea += saveArea.join(',')+',';
  }

  if (converseArea[converseArea.length-1] === ',') {
    // converseArea = converseArea[converseArea.length-2];
    converseArea = converseArea.slice(0, converseArea.length-1);
  }
  
  return converseArea;
}



export default App;